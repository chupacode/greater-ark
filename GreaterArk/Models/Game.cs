﻿using System.Collections.Generic;

namespace GreaterArk.Models
{
    public class Game
    {
        public string Name { get; set; }
        public IEnumerable<Film> Films { get; set; }
    }
}
