﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreaterArk.Models
{
    public class Medal
    {
        public string Group { get; set; }
        public string Name { get; set; }
        public int Count { get; set; }
    }
}
